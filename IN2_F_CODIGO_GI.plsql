create or replace FUNCTION IN2_F_CODIGO_GI (P_PNAME VARCHAR2)
RETURN NUMBER AS 
V_GI NUMBER;
BEGIN
  SELECT DECODE(LENGTH(TRIM(TRANSLATE(SUBSTR(p_pname,1,4), ' +-.0123456789', ' '))),1,SUBSTR(p_pname,2,4),NULL,SUBSTR(p_pname,1,4))
  INTO V_GI
  FROM DUAL; 
  RETURN V_GI;
END IN2_F_CODIGO_GI;